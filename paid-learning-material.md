Paid Learning Material
===

We can arrange you paid access to ..
* https://oreilly.com/ - Tens of 1000s of books, videos, interactive workshops on Software, Business, Design, Career Development
* https://audible.in - Millions of books available in audio format
* https://udemy.com - Pick the course you want us to buy for you
* https://manflowyoga.com - 1000s of videos on using yoga for physical fitness
* [https://iiba.org](https://www.iiba.org/my-iiba/membership/) - access to BABOK, a standard for business analysis. And 11,000 other books

Check with @tushar12123 or @rickysb and they will arrange a credential for you. **Have you found a paid learning resource not listed above? Please feel free to recommend and we would buy it for you.**
